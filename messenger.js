var IM = (function() {
    'use strict';

    function addEventListener(obj, evt, func) {
        if ('addEventListener' in obj){
            obj.addEventListener(evt, func, false);
        } else if ('attachEvent' in obj){//IE
            obj.attachEvent('on'+evt, func);
        }
    }

    function postMessage(obj, message, targetOrigin) {
        if ('postMessage' in obj) {
            return obj.postMessage(message, targetOrigin);
        }
        console.error('Impossible to post message.');
    }

    function onMessage(callback, origin) {
        var secureCallback = function (e) {
            if (e.origin == origin) {
                return callback(e);
            }

            console.warn('Message rejected from origin: '+e.origin);
        }

        addEventListener(window, 'message', secureCallback);
    }

    return {
        postMessage: postMessage,
        onMessage: onMessage
    }
})();
